module.exports = function (paths) {
    'use strict';

    return {
        module: {
            rules: [
                {
                    test: /\.(jpe?g|png|svg)$/i,
                    include: paths,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'images/[name]-[hash-8].[ext]',
                                publicPath: 'images/'
                            }
                        }
                    ]
                }
            ]
        }
    };
};