var Generator = require('yeoman-generator');

module.exports = class extends Generator {
    prompting() {
        return this.prompt([
            {
                type: 'input',
                name: 'publicPath',
                message: 'Public path',
                default: 'public',
                store: true
            },
            {
                type: 'input',
                name: 'moduleName',
                message: 'Module name',
                default: 'default' // Default to current folder name
            },
            {
                type: 'input',
                name: 'componentName',
                message: 'Component name',
                default: 'default' // Default to current folder name
            }
        ]).then((answers) => {
            this.publicPath = answers.publicPath;
            this.componentName = answers.componentName;
            this.moduleName = answers.moduleName;
        });
    }

    writing() {
        var component = this.componentName,
            module = this.moduleName,
            folderPath = this.publicPath + '/' + module;

        this.fs.copyTpl(
            this.templatePath('template.pug'),
            this.destinationPath(folderPath + '/template.pug'),
            { title: component }
        );

        this.fs.copyTpl(
            this.templatePath('component.js'),
            this.destinationPath(folderPath + '/' + component + '.component.js'),
            { componentName: component }
        );

        this.fs.copyTpl(
            this.templatePath('module.js'),
            this.destinationPath(folderPath + '/' + module + '.module.js'),
            {
                moduleName: module,
                componentName: component
            }
        );

        this.fs.copyTpl(
            this.templatePath('template.sass'),
            this.destinationPath(folderPath + '/template.sass'),
            { baseTag: component }
        );
    }

};

