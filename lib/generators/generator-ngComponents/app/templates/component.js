module.exports = {
    init: function (moduleName) {
        var templateHtml = require('./template.pug')();

        angular.module(moduleName)
            .component('<%= componentName %>Place', {
                template: templateHtml,
                controller: function () {

                }
            });
    }
};
