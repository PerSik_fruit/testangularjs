require('./template.sass');

var appComponent = require('./<%= componentName %>.component'),
    app = angular.module('<%= moduleName %>', []);

appComponent.init('<%= moduleName %>');

module.exports = app;


