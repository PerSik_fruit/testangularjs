var Generator = require('yeoman-generator');

module.exports = class extends Generator {
    prompting() {
        return this.prompt([
            {
                type: 'input',
                name: 'publicPath',
                message: 'Public path',
                default: 'public',
                store: true
            },
            {
                type: 'input',
                name: 'moduleName',
                message: 'Module name',
                default: 'default' // Default to current folder name
            },
            {
                type: 'input',
                name: 'serviceName',
                message: 'Service name',
                default: 'default' // Default to current folder name
            }
        ]).then((answers) => {
            this.publicPath = answers.publicPath;
            this.moduleName = answers.moduleName;
            this.serviceName = answers.serviceName;
        });
    }

    writing() {
        var component = this.componentName,
            module = this.moduleName,
            folderPath = this.publicPath + '/' + module;

        this.fs.copyTpl(
            this.templatePath('module.js'),
            this.destinationPath(folderPath + '/' + module + '.module.js'),
            {
                moduleName: module,
                componentName: component
            }
        );

    }
};

