
var app = angular.module('<%= moduleName %>', []);

app.factory('<%= serviceName %>', function () {
   return {
       init: (function () {
           console.log('INIT FACTORY - <%= serviceName %>')
       }())
   }
});

module.exports = app;


