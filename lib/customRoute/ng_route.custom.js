'use strict';

var route = {
    config: [],
    componentLinks: [],
    modules: [],

    setModules: function (modules) {
      this.modules = modules;
    },

    getModules: function () {
        return this.modules;
    },

    getConfig: function () {
        return this.config
    },

    generateConfig: function (config) {
        var componentLinks = config.map(function (component) {
                return {
                    url: component.replace('App', ''),
                    pathName: component.toUpperCase()
                }
            }),
            paths;

        this.componentLinks = componentLinks;

        paths = componentLinks.map(function (path) {
            return {path: path.url, name: path.pathName, component: path.url + 'Place'}
        });

        this.config = paths;
    }
};


module.exports = route;