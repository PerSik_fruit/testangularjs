require('./template.sass');

require('angular');
require('angular-route');
require('@angular/router/angular1/angular_1_router');
require('@angular/router/angular1/ng_route_shim');

var routeLinks = require('../routeLinks/route-links.module');
var maze = require('../maze/maze.module');

var Route = require('../../../lib/customRoute/ng_route.custom');

Route.setModules([
    'ngRoute',
    'ngComponentRouter',
    routeLinks.name,
    maze.name
]);

var config = [
    maze.name
];

Route.generateConfig(config);

var app = {
    initialize: function () {
        var appHtml = require('./template.pug')();

        angular.module('nonameApp', Route.getModules())
            .config(function ($locationProvider) {
                $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                });
            })
            .value('$routerRootComponent', 'app')
            .component('app', {
                template: appHtml,
                controller: function ($http, $location) {
                    var self = this;

                    self.mainInfo = {
                        appName: 'Maze'
                    };

                    self.compLinks = Route.componentLinks;
                    console.log(Route.getConfig());
                },
                $routeConfig: Route.getConfig()
            });
    }
};

module.exports = app;
