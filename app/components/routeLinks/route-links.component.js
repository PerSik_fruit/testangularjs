module.exports = {
    init: function () {
        var templateHtml = require('./template.pug')();

        angular.module('routeApp').component('routePlace', {
            template: templateHtml,
            controller: function () {
                var self = this;

                self.links = [
                    {
                        url: '#',
                        path: 'one'
                    },
                    {
                        url: '#',
                        path: 'two'
                    }
                ];

                self.main = {
                    appName: 'Route component'
                }
            },
            bindings: {
                links: '<',
                main: '<'
            }
        });
    }
};
