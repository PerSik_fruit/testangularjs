require('./template.sass');
var route = require('./route-links.component')

var routeApp = angular.module('routeApp', []);

route.init();

module.exports = routeApp;