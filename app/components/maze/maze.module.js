require('./template.sass');

var appComponent = require('./components/controlPanel/controlPanel.component'),
    app = angular.module('maze', []);

appComponent.init('maze');

module.exports = app;


