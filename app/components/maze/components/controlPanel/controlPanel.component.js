module.exports = {
    init: function (moduleName) {
        var templateHtml = require('./template.pug')();

        angular.module(moduleName)
            .component('mazePlace', {
                template: templateHtml,
                controller: function () {

                }
            });
    }
};
